# Home

## Hello, new student!

![](./images/week01/imagen.png)

## Welcome to your new Fab Academy site

Greetings! I am Fernando, a passionate industrial engineer and teacher specialized in the Occupational Health and Safety Management career. My goal is to contribute to the well-being and safety of people in their work environments by applying my knowledge and experience in the industry. As an educator, I am proud to share my knowledge and guide future professionals so that they become leaders committed to the prevention of occupational risks and the continuous improvement of work environments. I am always open to learning and collaborating on projects that promote safety, efficiency and care for human capital in any industrial field. Together we can create a safer and healthier future of work for all!




