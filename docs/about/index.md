# About me


I am 32 years old and born in Arequipa. I am Fernando, an experienced occupational health and safety instructor with an outstanding track record of 11 years in the field. My experience has been forged through my dedication in the mining sector, working with renowned mining units such as Cerro Verde, Las Bambas, Marcobre and Hudbay. Using my specialized knowledge and effective communication skills, I have played a key role in promoting and enforcing safe practices in demanding work environments. My passion for guaranteeing a safe and healthy work environment has made me a benchmark in the industry, demonstrating an unwavering commitment to the safety of workers and the success of the mining projects in which I have participated.
![](../images/week01/cerroverde.jpg)

My hobby is playing sports, I like cycling. On weekends I go out with my friends on the outskirts of the city, I participate in some competitions, in Arequipa the competition is tough and the participants are very good, I also like to run through Chilina since you can find very beautiful and pleasant landscapes.
I believe that sport gives us health and keeps us active.

![](../images/week01/chilina.jpg)
![](../images/week01/bici.jpg)