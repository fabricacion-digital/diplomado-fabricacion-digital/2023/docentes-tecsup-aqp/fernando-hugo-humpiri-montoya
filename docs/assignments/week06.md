# 6. It began with silicone molding with some figures

A design was made to make a laser cut and use it in the mold to make the application.

![](../images/week01/mold1.jpg)

Clay was used to make the base and place the figures, there were problems in the location of the acrylic parts but it was joined again with silicone to correct the error found

![](../images/week01/mold2.jpg)

Some element is used to calculate the amount of silicone mixture with catalyst that would be applied, in this case we use rice.

![](../images/week01/mold3.jpg)

The mixture of silicone with catalyst was made, in a proportion of 2 grams of catalyst per 100 grams of silicone.
In this case, 490 grams of silicone and 10 grams of catalyst were used and the casting was carried out.

![](../images/week01/mold4.png)

The second silicone application process was carried out and it was left to dry for its subsequent application of resin, which has not yet been completed.