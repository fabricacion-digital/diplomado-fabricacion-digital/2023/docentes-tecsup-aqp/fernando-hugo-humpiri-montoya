# 8. Embedded programming

To start this week, we start by knowing what arduino is, introduction and application

![](../images/week01/arduino.jpg) 

![](../images/week01/arduino2.jpg) 

For the different functions to use, we go to documentation and then references to be able to perform the functions that need to be applied.

![](../images/week01/arduinoref.png) 

With this we already have the arduino installed and ready to be used

![](../images/week01/arduinolist.png) 

After having it installed, we can define the board that we will use, in this case Arduino mega or mega 2560

![](../images/week01/arduinoboard.png) 

We also verify the port, from device manager we corroborate it and in Arduino we confirm so that it can work correctly.

![](../images/week01/arduinoport.png) 

We apply some functions such as: pinMode, digitalWrite and delay

![](../images/week01/arduinofunciones.png)

Installing the Arduino ESP32 core

![](../images/week01/preference.png)

Complete the “Additional Dashboard Admin URLs” field with the following.

![](../images/week01/adicional.png)

We install esp32 by Espressif Systems

![](../images/week01/espressif.png)

After completing the installation, we go to tools and change Board to esp32 and then "ESP32 DEV Module"

![](../images/week01/devmodule.png)

We also corroborate the port

![](../images/week01/port.png)

Once the "ESP32" is configured, we can apply the commands

![](../images/week01/si.jpeg)

![](../images/week01/esp32.png)

![](../images/week01/video1.mp4)
