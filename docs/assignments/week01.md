# 1. Principles and Practices, Project Management

MY FINAL PROJECT IDEA

My project idea is to develop a cube to perform CPR maneuver practices, focusing mainly on hands-only CPR, it will work to improve some existing devices, therefore I decided to improve the "CPR CUBE" experience.


![](../images/week01/proyectocubo.jpg)



Investigation¶

PROBLEM: Application and execution of CPR practices without a reliable result when knowing if it is being applied correctly or not.

SOLUTION: Design of a cube for CPR practice, simulating the compressions to be performed and establishing a result after the experience of each participant.

<b>What will I do?¶</b>

I will improve the experience and application of a device for the practice of a subject as important as the prevention and application of CPR maneuvers, according to the guidelines of the AHA (American Heart Association) the time of help to a person with cardiac arrest It is 5 minutes, knowing the CPR maneuver and its correct application could be the difference between the life and death of a person.

Currently there are devices that help us in the practice of this maneuver, however due to time factors or simple perception of the evaluator it is not possible to determine a correct application in terms of depth and speed.

<b>Who will use it?</b>¶

The device could be used in different areas and settings, mainly for training and knowledge for the dissemination of knowledge of the CPR maneuver, giving priority to the maneuver performed only with the hands.