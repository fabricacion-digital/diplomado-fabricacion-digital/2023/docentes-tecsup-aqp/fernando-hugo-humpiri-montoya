# 3. Computer-aided design

In this week's hands-on, I first characterized the parameters of my laser cutter and then designed, laser cut, and documented my desktop organizer. In addition, I also made a sticker with different colors of the Pokémon character called Pikachu.

The laser cutting software is RDWorksV8. The cutter can read AI\BMP\GIF\JPEG\PCX\TGA \CDR\DWG\TIFF\PLT\DXF files.

![](../images/week01/works.png)

The handling of the laser cutter is shown in the following images and video

![](../images/week01/cortadoralaser1.jpg)

![](../images/week01/cortadoralaser2.jpg)

![](../images/week01/cortadoralaser3.jpg)

After carrying out the cutting process, the complete assembly of the element began.

![](../images/week01/organizador1.jpg)

![](../images/week01/organizador2.jpg)

The process of cutting vinyl in different colors and cutting acrylic was also carried out, as well as folding it to paste the vinyl and the shape.

![](../images/week01/acrilico1.png)

![](../images/week01/corel1.png)

![](../images/week01/roland.jpg)

![](../images/week01/roland2.jpg)

![](../images/week01/vinilterminado.jpg)