# 1. Project management

This space will be dedicated to describing the details of how learning evolves as we advance in Fab Academy training.

1. creating a walkthrough

2. update of the information in GITLAB.

3. Installing Git.
We start with the Git download through the following Link: https://git-scm.com/download/win choosing the Git installation file for 64-bit windows

![](../images/week01/1.png)

4. Install Sublime Text: Download and install Sublime Text on your local machine from the official website.

![](../images/week01/2.png)

 Select Sublime Text as GitLab Editor: After logging in to GitLab, navigate to your profile settings and find the "Editor" section. Choose "Sublime Text" as your preferred editor for GitLab.

 Git Bash: If you haven't already, download and install Git Bash on your local machine. This will provide you with a command-line interface to work with Git.


5. git clone https://gitlab.com/fabricacion-digital/diplomado-fabricacion-digital/2023/docentes-tecsup-aqp/fernando-hugo-humpiri-montoya.git

Then the cloning of the information will be carried out to have a repository, a folder is created on our disk.

![](../images/week01/3.png)
![](../images/week01/4.png)

6. We generate the SSH key:
   ssh-keygen -t rsa -b 2048 -C "fhumpiri@tecsup.edu.pe"

7. Copy SSH Key to GitLab: Run the following command to copy the generated SSH key and paste it into your GitLab account settings under "SSH Keys": cat ~/.ssh/id_rsa.pub

![](../images/week01/5.png)
![](../images/week01/6.png)

Configure Local Username: Set up your local Git configuration with your username using the following command: git config --global user.name "fhumpiri"

Configure GitLab Email: Configure your GitLab email using the following command: git config --global user.email "fhumpiri@tecsup.edu.pe"

8. Initialize Git Repository: Inside the cloned repository folder, run the following command to initialize a new Git repository:

9. Now you have successfully added the project from GitLab to your local machine, configured your Git settings, and initialized a local Git repository. You can start making changes to your project and use Git commands to manage version control and collaborate with others on GitLab.