# 5. The use of the CNC cutter and the correct application of the design in FUSION 360 were applied

![](../images/week01/FUSION1.png)

![](../images/week01/CNC1.jpg)

Then, what was learned was applied to elaborate a small project for a bench and the modeling and subsequent cutting was carried out on the CNC.

The configuration was made to start with the cut

![](../images/week01/cnc2.png)

![](../images/week01/CNC3.png)

![](../images/week01/CNC4.jpg)
